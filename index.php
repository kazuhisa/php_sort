<?php
require 'vendor/autoload.php';
require('user.php');
use League\Csv\Reader;

$user_list = [];

$reader = Reader::createFromPath('user.csv');
foreach ($reader as $index => $row) {
  if ($index == 0){
    continue;
  }
  array_push($user_list, new User($row));
}

function age_order($a, $b){
  return $a->age - $b->age;
}

usort($user_list, "age_order");

foreach($user_list as $user){
  echo "$user->name:$user->age\n";
}