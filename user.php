<?php
class User
{
  public $name;
  public $kana;
  public $email;
  public $gender;
  public $age;

  public function __construct($row) {
    $this->name = $row[0];
    $this->kana = $row[1];
    $this->email = $row[2];
    $this->gender = $row[3];
    $this->age = $row[4];
  }
}